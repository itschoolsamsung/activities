package com.example.al.anotherscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
	public static final int RES_DIALOG = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void btnAboutClick(View view) {
		Intent intent = new Intent(MainActivity.this, AboutActivity.class);
		startActivity(intent);
	}

	public void btnInfoClick(View view) {
		Intent intent = new Intent(MainActivity.this, InfoActivity.class);
		startActivity(intent);
	}

	public void btnDialogClick(View view) {
//		((EditText)findViewById(R.id.editText)).setText("Qu");
		Intent intent = new Intent(MainActivity.this, DialogActivity.class);
		startActivityForResult(intent, RES_DIALOG);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case RES_DIALOG:
				if (RESULT_OK == resultCode) {
					String msg = data.getStringExtra(DialogActivity.MSG_OK);
					((EditText)findViewById(R.id.editText)).setText(msg);
				}
				if (RESULT_CANCELED == resultCode) {
					((EditText)findViewById(R.id.editText)).setText("cancelled");
				}
				break;
		}
	}
}
