package com.example.al.anotherscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class DialogActivity extends Activity {
	public static final String MSG_OK = "Message.OK";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dialog);
	}


	@Override
	protected void onDestroy() {
		super.onDestroy();

	}

	public void btnOKClick(View view) {
		Intent intent = new Intent();
		intent.putExtra(MSG_OK, "OK");
		setResult(RESULT_OK, intent);
		finish();
	}

	public void btnCancelClick(View view) {
		setResult(RESULT_CANCELED);
		finish();
	}
}
