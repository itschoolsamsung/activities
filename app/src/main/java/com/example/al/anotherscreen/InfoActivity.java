package com.example.al.anotherscreen;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class InfoActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
	}

	public void btnCloseClick(View view) {
		finish();
	}
}
